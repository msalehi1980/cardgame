//
//  Card.h
//  Card Game
//
//  Created by giatec on 2017-02-07.
//  Copyright © 2017 Mustafa Salehi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
@property (nonatomic) NSString *contents;
@property (nonatomic, getter=isChosen) BOOL chosen;
@property (nonatomic, getter=isMatched) BOOL matched;

- (int) match:(NSArray *) otherCards;

@end
