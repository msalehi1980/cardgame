//
//  CardGameViewController.m
//  Card Game
//
//  Created by giatec on 2017-02-08.
//  Copyright © 2017 Mustafa Salehi. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"
#import "CardMatchingGame.h"

@interface CardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (nonatomic)  Deck *myDeck;
@property (nonatomic) CardMatchingGame *game;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;

@end


@implementation CardGameViewController

- (void) viewDidLoad{
    self.flipsLabel.text = @"Score = ";
}

- (CardMatchingGame *) game{
    if(!_game) _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                         usingDeck:[self createDeck]];
    return _game;
}

- (Deck *) createDeck{
    return [[PlayingCardDeck alloc]init];
}

//- (Deck *) myDeck {
//    if(!_myDeck) _myDeck = [self createDeck];
//    return _myDeck;
//}


//- (void) setFlipCount: (int) flipCount {
//    _flipCount = flipCount ;
//    self.flipsLabel.text= [NSString stringWithFormat:@"Flips: %d",self.flipCount];
//    NSLog(@"flipsCoung = %d",self.flipCount);
//}





- (IBAction)touchCardButton:(UIButton *)sender {
    NSUInteger choosenButtonIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:choosenButtonIndex];
    [self updateUI];
    self.flipsLabel.text = [NSString stringWithFormat:@"Score = %ld",self.game.score];
//    if ([sender.currentTitle length])
//    {
//        UIImage *cardImage = [UIImage imageNamed:@"cardback"];
//        [sender setBackgroundImage:cardImage
//                          forState:UIControlStateNormal];
//        [sender setTitle:@"" forState:UIControlStateNormal];
//        
//    }else{
//        Card *card = [self.myDeck drawRandomCard];
//        if (card){
//            NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:card.contents];
//            
//            if ([card.contents containsString:@"♦︎"] || [card.contents containsString:@"♥︎"] ){
//            
//            [content setAttributes:@{NSForegroundColorAttributeName: [UIColor redColor]}
//                                     range:NSMakeRange (0, [content length])];
//                //why selecting only the last charcter doesn't show the suit properly!
//            }
//            else
//            {
//                [content setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}
//                                 range:NSMakeRange (0, [content length])];
//
//            }
//            [sender setBackgroundImage:[UIImage imageNamed:@"cardfront"]
//                              forState:UIControlStateNormal];
//            //[sender setBackgroundColor: [UIColor whiteColor]];
//            
//            [sender setAttributedTitle:content forState:UIControlStateNormal];
//        self.flipCount++;
//        }

}

- (void) updateUI{
    for (UIButton *cardButton in self.cardButtons){
        NSUInteger  cardButtonIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardButtonIndex];
        [cardButton setAttributedTitle:[self titleForCard:card]
                              forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgroundImageForCard:card]
                              forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
    }
}

- (NSAttributedString *) titleForCard:(Card*) card{

    NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:card.contents];
    if ([card.contents containsString:@"♦︎"] || [card.contents containsString:@"♥︎"] ){

        [content setAttributes:@{NSForegroundColorAttributeName: [UIColor redColor]}
                         range:NSMakeRange (0, [content length])];
        //why selecting only the last charcter doesn't show the suit properly!
    }
    else
    {
        [content setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}
                     range:NSMakeRange (0, [content length])];

    }
    return card.isChosen? content : [[NSMutableAttributedString alloc]initWithString:@""];
}

- (UIImage *) backgroundImageForCard: (Card *) card{
    return [UIImage imageNamed: card.isChosen ? @"cardfront" : @"cardback"];
}
    
@end
