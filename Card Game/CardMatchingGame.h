//
//  CardMatchingGame.h
//  Card Game
//
//  Created by giatec on 2017-02-10.
//  Copyright © 2017 Mustafa Salehi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

//designated initializer
- (instancetype) initWithCardCount:(NSUInteger) count
                         usingDeck: (Deck *) deck;

- (void) chooseCardAtIndex: (NSUInteger) index;
- (Card *) cardAtIndex: (NSUInteger) index;

@property   (nonatomic,readonly) NSInteger score;

@end
