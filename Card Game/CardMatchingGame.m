//
//  CardMatchingGame.m
//  Card Game
//
//  Created by giatec on 2017-02-10.
//  Copyright © 2017 Mustafa Salehi. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()
@property (nonatomic,readwrite) NSInteger score;
@property (nonatomic) NSMutableArray *cards; //of Card
@end


@implementation CardMatchingGame

static const int MATCH_BONUS = 4;
static const int MISMATCH_PENALTY = 2;
static const int COST_TO_CHOOSE = 1;

- (NSMutableArray *) cards{
    if(!_cards) _cards=[[NSMutableArray alloc]init];
    return _cards;
}

-(instancetype) init{
    return nil;
}

- (instancetype) initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck {
    self = [super init] ;
    if (self){
        for (int i=0;i<count; i++){
            Card *card = [deck drawRandomCard];
            if (card){
               [self.cards addObject:card];
            }
            else{
                self = nil;
            }
            
        }
        
    }
    return self;
}

-(Card *)cardAtIndex:(NSUInteger)index{
    
    return (index < [self.cards count]) ? self.cards[index] : nil;
}

- (void)chooseCardAtIndex:(NSUInteger)index {
    Card *card = [self cardAtIndex:index];
    
    if (!card.isMatched){
        if (card.isChosen){
            card.chosen = NO;   //deselect the card if not matched
        }
        else{
            // match against another card
            for (Card *otherCard in self.cards){
                if (otherCard.isChosen && !otherCard.isMatched ){
                    int matchScore = [card match:@[otherCard]];
                    if(matchScore){
                        self.score += matchScore * MATCH_BONUS; //reward
                        otherCard.matched = YES;
                        card.matched = YES;
                    }
                    else{
                        self.score -= MISMATCH_PENALTY; //penalty
                        otherCard.chosen = NO; //deselct the other card
                    }
                    
                }
            }
            self.score -= COST_TO_CHOOSE;
            card.chosen = YES; //select the card
        }
    }
}

@end
