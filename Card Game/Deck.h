//
//  Deck.h
//  Card Game
//
//  Created by giatec on 2017-02-07.
//  Copyright © 2017 Mustafa Salehi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject
- (void) addCard: (Card *) card atTop: (BOOL)atTop;
- (void) addCard:(Card *) card;
- (Card *) drawRandomCard;

@end
