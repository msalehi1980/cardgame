//
//  Deck.m
//  Card Game
//
//  Created by giatec on 2017-02-07.
//  Copyright © 2017 Mustafa Salehi. All rights reserved.
//

#import "Deck.h"

@interface Deck()

@property (nonatomic) NSMutableArray *cards; //Deck of cards

@end

@implementation Deck

- (NSMutableArray *) cards{  //This is the getter of cards
    if (!_cards) _cards = [[NSMutableArray alloc]init]; //this is to allocate
    return _cards;
}


- (void) addCard:(Card *)card atTop:(BOOL)atTop{
    
    if (atTop){
        [self.cards  insertObject:card atIndex:0];
    }
    else{
        [self.cards addObject:card];
    }
}


- (void) addCard:(Card *)card{
    [self addCard:card atTop:NO];
}


- (Card *) drawRandomCard{
    
    Card *randomCard = nil;
    
    if ([self.cards count]) {
    
        unsigned index = arc4random() % [self.cards count];
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    return randomCard;
}


@end
