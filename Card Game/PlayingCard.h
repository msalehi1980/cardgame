//
//  PlayingCard.h
//  Card Game
//
//  Created by giatec on 2017-02-07.
//  Copyright © 2017 Mustafa Salehi. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card
@property   (nonatomic) NSString    *suit;
@property   (nonatomic) NSUInteger  rank;
+ (NSArray *) validSuits;
+ (NSUInteger ) maxRank;

@end
