//
//  PlayingCard.m
//  Card Game
//
//  Created by giatec on 2017-02-07.
//  Copyright © 2017 Mustafa Salehi. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

- (int)match:(NSArray *)otherCards{
    int score = 0;
    if ([otherCards count]==1){ //matching only single card for now
        PlayingCard *otherCard = [otherCards firstObject];
        if(otherCard.rank == self.rank){
            score = 4;
        }else if ([otherCard.suit isEqualToString:self.suit]){
            score = 1;
        }
    }
    return score;
}

- (NSString *) contents {

    return [NSString stringWithFormat:@"%@ %@",[PlayingCard rankStrings][self.rank],self.suit];
}



+ (NSArray *) rankStrings {
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6"
             ,@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

+ (NSArray *) validSuits {
    return @[@"♥︎",@"♦︎",@"♣︎",@"♠︎"];
}
@synthesize suit = _suit;
- (void) setSuit:(NSString *) suit {
    if ([ [PlayingCard validSuits] containsObject: suit])
    {
        _suit = suit;
    }
}

- (NSString *) suit {
    return _suit ? _suit :@"?";
}

+ (NSUInteger ) maxRank {
    return [[self rankStrings] count ]-1;
}

- (void) setRank:(NSUInteger)rank{
    if (rank <= [PlayingCard maxRank]){
        _rank = rank;
    }
}


@end
